<?php
# statusmap.php
# Simple status map for Nagios 3 written in PHP
# 
# On Debian I normally put this file under /usr/share/nagios4/htdocs/map.php
# and I access it on https://nagios.example.com/nagios4/map.php
# using HTTP authentication
# 
# Andres Hernandez - tonejito
# Released under the BSD license

#	= ^ . ^ =

$refresh = 60;
$date = date("U");
$width = 1920;
$height = 1080;
$map = "/cgi-bin/nagios4/statusmap.cgi?host=all&createimage&time=$date&canvas_x=0&canvas_y=0&canvas_width=$width&canvas_height=$height&max_width=0&max_height=0&layout=5&layermode=exclude";
header("Refresh: ".$refresh."; url=".$_SERVER['PHP_SELF']);
flush();
?>
<!DOCTYPE html>
<!--	= ^ . ^ =	-->
<html>
 <head>
  <title>Nagios</title>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8" >
  <meta http-equiv="Refresh" content="<?= $refresh; ?>; url=<?= $_SERVER['PHP_SELF']; ?>" >
  <style type="text/css">
   html,body{ height: 99%; padding: 0 0 0 0; text-align: center; }
   body
   {
     background-color: DimGray;
     /* Old browsers */
     background: #c0c0c0;
     /* FF3.6-15 */
     background: -moz-radial-gradient(center, ellipse cover, #c0c0c0 0%, #000000 100%);
     /* Chrome10-25,Safari5.1-6 */
     background: -webkit-radial-gradient(center, ellipse cover, #c0c0c0 0%,#000000 100%);
     /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
     background: radial-gradient(ellipse at center, #c0c0c0 0%,#000000 100%);
     /* IE6-9 fallback on horizontal gradient */
     filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#c0c0c0', endColorstr='#000000',GradientType=1 );
   }
   img { margin: 0 auto; width: auto; height: auto; max-width: 100%; max-height: 100%; }
  </style>
 </head>
<?php flush(); ?>
 <body>
  <map name='statusmap'>
   <area shape='rect' coords='0,0,<?= $width; ?>,<?= $height; ?>' href='/nagios4/?corewindow=/cgi-bin/nagios4/statusmap.cgi?host=all'>
  </map>
  <img src='<?= $map; ?>' width=<?= $width; ?> height=<?= $height; ?> border=0 name='statusimage' useMap='#statusmap'>
 </body>
</html>
